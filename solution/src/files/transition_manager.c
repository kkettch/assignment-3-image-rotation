/*
 * transition_manager.c - описание структуры изображения и функции для работы с ней
 */

#include "bmp.h"
#include "image_manager.h"
#include "used_enums.h"
#include <stdio.h>


#define bmp_identificator 0x4D42
#define bit_count 24
#define bi_size 40
#define padding ((4 - ((img->width) * 3) % 4) % 4)

enum read_status from_bmp(FILE* in, struct image* img) {

    //считываем файл в структуру bmp_header
    struct bmp_header header;

    //проверка на ошибку чтения файла
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_ERROR;
    }

    //проверяем на ошибки
    if (header.bfType != bmp_identificator) {
        return READ_ERROR;
    }

    //выделяем память для структуры image
    *img = create_image(header.biWidth, header.biHeight);

    //заполняем массив пикселей
    for (uint64_t y = 0; y < header.biHeight; ++y) {

        for (uint64_t x = 0; x < header.biWidth; ++x) {
            //проверка на ошибку чтения файла
            if (fread(&(img->data[y * img->width + x]), sizeof(struct pixel), 1, in) != 1) {
                return READ_ERROR;
            }
        }
        //проверка на ошибку
        if (fseek(in, (long) padding, SEEK_CUR) != 0) {
            return READ_ERROR;
        }
    }

    //все получилось
    return READ_OK;
}

enum write_status to_bmp(FILE* out, const struct image* img) {

    //формируем структуру заголовка
    struct bmp_header bmp_header;
    bmp_header.bfType = bmp_identificator;
    bmp_header.bfileSize = sizeof(struct bmp_header) + img->width * img->height * sizeof(struct pixel);
    bmp_header.bfReserved = 0;
    bmp_header.bOffBits = sizeof(struct bmp_header);
    bmp_header.biSize = bi_size;
    bmp_header.biWidth = (uint32_t)img->width;
    bmp_header.biHeight = (uint32_t)img->height;
    bmp_header.biPlanes = 1;
    bmp_header.biBitCount = bit_count;
    bmp_header.biCompression = 0;
    bmp_header.biSizeImage = 0;
    bmp_header.biXPelsPerMeter = 0;
    bmp_header.biYPelsPerMeter = 0;
    bmp_header.biClrUsed = 0;
    bmp_header.biClrImportant = 0;

    //записываем заголовок в файл
    fwrite(&bmp_header, sizeof(struct bmp_header), 1, out);

    //отправляем массив пикселей
    for (uint64_t y = 0; y < img->height; ++y) {

        for (uint64_t x = 0; x < img->width; ++x) {
            //проверка на ошибку записи
            if (fwrite(&img->data[y * img->width + x], sizeof(struct pixel), 1, out) != 1) {
                return WRITE_ERROR;
            }
        }
        //проверка на ошибку паддинга
        for (uint32_t i = 0; i < padding; ++i) {
            if (fputc(0, out) == EOF) {
                return WRITE_ERROR;
            }
        }
    }

    //все получилось
    return WRITE_OK;
}



