/*
 * file_manager.c - открытие/закрытие файлов
 */

#include "used_enums.h"
#include <stdio.h>


//открываем файл для чтения
enum file_open_status open_file_for_reading(const char* filename, FILE** file) {
    *file = fopen(filename, "rb");
    if (!*file) {
        return FILE_OPEN_ERROR;
    }
    return FILE_OPEN_OK;
}

//открываем файл для записи
enum file_open_status open_file_for_writing(const char* filename, FILE** file) {
    *file = fopen(filename, "wb");
    if (!*file) {
        return FILE_OPEN_ERROR;
    }
    return FILE_OPEN_OK;
}

//закрываем файл :-(
enum file_close_status close_file(FILE* file) {
    if (fclose(file) == EOF) {
        return FILE_CLOSE_ERROR;
    }
    return FILE_CLOSE_OK;
}

