/*
 * image_manager.c - Описание структуры изображения и функции для работы с ней
 */

#include "image_manager.h"
#include <stdio.h>
#include <stdlib.h>



//освобождение памяти
void destroy_image(struct image* image) {
    if (image->data) {
        free(image->data);
    }
}

//выделение памяти на основе переданных ширины и высоты, возвращает указатель на выделенную структуру
struct image create_image(uint64_t width, uint64_t height) {

    struct image image = {0};
    image.width = width;
    image.height = height;
    image.data = malloc(width * height * sizeof(struct pixel));

    //проверка на out of memory
    if (image.data == NULL) {
        fprintf(stderr, "Ошибка выделения памяти для изображения\n");
        exit(1);
    }

    return image;
}

//получение структуры пикселя по значению (x; y)
struct pixel get_pixel(const struct image* image, uint64_t x, uint64_t y) {
    return image->data[y * image->width + x];
}


//установка значения пикселя в изображение по значению (x; y)
void set_pixel(struct image* image, uint64_t x, uint64_t y, struct pixel pixel) {
    image->data[y * image->width + x] = pixel;
}

struct image copy_image(const struct image* src) {
    struct image dest = {0};

    dest.width = src->width;
    dest.height = src->height;

    dest.data = malloc(dest.width * dest.height * sizeof(struct pixel));
    if (!dest.data) {
        return dest;
    }

    for (uint64_t i = 0; i < dest.width * dest.height; ++i) {
        dest.data[i] = src->data[i];
    }

    return dest;
}























