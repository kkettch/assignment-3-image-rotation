/*
 * rotate.c - реализация поворота изображения
 */

#include "image_manager.h"
#include <stdlib.h>

struct image rotate( struct image const source ) {
    struct image rotated = create_image(source.height, source.width);
    for (size_t i = 0; i < source.height; i++) {
        for (size_t j = 0; j < source.width; j++) {
            struct pixel pixel = get_pixel(&source, j, i);
            set_pixel(&rotated, i, source.width - j - 1, pixel);
        }
    }
    return rotated;
}

