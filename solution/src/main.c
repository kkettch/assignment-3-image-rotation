#include "file_manager.h"
#include "rotate.h"
#include "transition_manager.h"
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {

    //проверка аргументов функции
    if (argc != 4) {
        fprintf(stderr, "Для использования передайте аргументы: %s <source-image> <transformed-image> <angle>", argv[0]);
        return 1;
    }

    //проверка значения угла
    int32_t angle = atoi(argv[3]);
    angle = (angle % 360 + 360) % 360;
    if (angle % 90 != 0) {
        fprintf(stderr, "Введите число, кратное 90! Полученное число: %d\n", angle);
        return ANGLE_ERROR;
    }

    //создание переменных для хранения промжуточных результатов
    FILE* file_in = NULL;
    FILE* file_out = NULL;
    struct image image = {0};


    //проверка открытия файла для записи
    if (open_file_for_reading(argv[1], &file_in) != FILE_OPEN_OK) {
        fprintf(stderr, "Ошибка открытия файла для чтения!\n");
        return FILE_OPEN_ERROR;
    }

    //проверка чтения bmp-структуры
    if (from_bmp(file_in, &image) != READ_OK) {
        fprintf(stderr, "Ошибка формирования структуры image\n");
        destroy_image(&image);
        return READ_ERROR;
    }

    //копирование изначальной структуры image
    struct image result = copy_image(&image);

    //проверка на выделение памяти для массива пикселей
    if (result.data == NULL) {
        fprintf(stderr, "Ошибка выделения памяти для копии изображения\n");
        destroy_image(&image);
        destroy_image(&result);
    }

    //поворот изображения
    for (size_t i = 0; i < (angle / 90); i++) {
        struct image tmp = rotate(result);
        destroy_image(&result);
        result = tmp;
    }

    //открытие файла для записи результа
    if (open_file_for_writing(argv[2], &file_out) != FILE_OPEN_OK) {
        fprintf(stderr, "Ошибка открытия файла для записи!\n");
        return FILE_OPEN_ERROR;
    }

    //формирование bmp-заголовка и результата
    if (to_bmp(file_out, &result) != WRITE_OK) {
        fprintf(stderr, "Ошибка формирования структуры bmp\n");
        destroy_image(&result);
        destroy_image(&image);
        return WRITE_ERROR;
    }

    //освобождение памяти
    destroy_image(&image);
    destroy_image(&result);
    close_file(file_in);
    close_file(file_out);

    //ура все закончилось
    return 0;
}
