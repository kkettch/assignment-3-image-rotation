#ifndef IMAGE_MANAGER_H
#define IMAGE_MANAGER_H

#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void destroy_image(struct image* image);
struct image create_image(uint64_t width, uint64_t height);
struct pixel get_pixel(const struct image* image, uint64_t x, uint64_t y);
void set_pixel(struct image* image, uint64_t x, uint64_t y, struct pixel pixel);
struct image copy_image(const struct image* src);

#endif // IMAGE_MANAGER_H


