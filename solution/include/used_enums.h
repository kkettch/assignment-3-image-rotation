#ifndef IMAGE_TRANSFORMER_USED_ENUMS_H
#define IMAGE_TRANSFORMER_USED_ENUMS_H

enum file_open_status {
    FILE_OPEN_OK = 0,
    FILE_OPEN_ERROR
};

enum file_close_status {
    FILE_CLOSE_OK = 0,
    FILE_CLOSE_ERROR
};

enum read_status  {
    READ_OK = 0,
    READ_ERROR
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum angle_status {
    ANGLE_ERROR
};

#endif //IMAGE_TRANSFORMER_USED_ENUMS_H
