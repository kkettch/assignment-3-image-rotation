#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H

#include "used_enums.h"
#include <stdio.h>

enum file_open_status open_file_for_reading(const char* filename, FILE** file);

enum file_open_status open_file_for_writing(const char* filename, FILE** file);

enum file_close_status close_file(FILE* file);

#endif // FILE_MANAGER_H

