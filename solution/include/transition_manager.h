#ifndef IMAGE_TRANSFORMER_TRANSITION_MANAGER_H
#define IMAGE_TRANSFORMER_TRANSITION_MANAGER_H

#include "image_manager.h"
#include "used_enums.h"
#include <stdio.h>

enum read_status from_bmp(FILE* in, struct image* image);
enum write_status to_bmp(FILE* out, const struct image* image);

#endif //IMAGE_TRANSFORMER_TRANSITION_MANAGER_H
