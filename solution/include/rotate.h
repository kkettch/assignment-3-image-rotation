#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#include "image_manager.h"

struct image rotate( struct image const source );

#endif //IMAGE_TRANSFORMER_ROTATE_H
